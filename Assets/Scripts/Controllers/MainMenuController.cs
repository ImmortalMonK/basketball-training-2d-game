﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	private Animator mainAnim, ballsAnim;

	void Awake(){
		mainAnim = GameObject.Find ("Main Holder").GetComponent<Animator> ();
		ballsAnim = GameObject.Find ("Balls Holder").GetComponent<Animator> ();
	}

	public void PlayGame(){
		Application.LoadLevel ("Gameplay");
	}

	public void SelectBall(){//при нажатии на кнопку Balls будет воспроизводиться анимация меню и мячей
		mainAnim.Play ("FadeOut");
		ballsAnim.Play ("FadeIn");
	}
	public void BackToMenu()
	{
		mainAnim.Play ("FadeIn");
		ballsAnim.Play ("FadeOut");
	}

	public void QuitGame(){
		Application.Quit ();
	}
}
