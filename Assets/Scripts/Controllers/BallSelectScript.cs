﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BallSelectScript : MonoBehaviour {

	private List<Button> buttons = new List<Button> ();
	void Awake(){

		GetButtonsAndAddListeners ();
	}

	void GetButtonsAndAddListeners()
	{
		GameObject[] btns = GameObject.FindGameObjectsWithTag ("MenuBall");// выьерет из списка объект, 
		//который пренадлежит тагу MenuBall
		for (int i = 0; i < btns.Length; i++) {
			buttons.Add (btns [i].GetComponent<Button> ());
			buttons[i].onClick.AddListener (() => SelectABall());
		}
	}

	public void SelectABall()// это функция - Listener
	{
		int index = int.Parse (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);
		// эта функция предоставит нам имя выбранного объекта в кейсе( в данном случае мячи от 0 до 9
		// это функция осуществит перенос строки в целое число. также используем индекс для понимания, какой мяч выбран
		//inform Game Manager which ball is selected
		Debug.Log ("The Selected index is " + index);

		if (GameManager.instance != null) {
			GameManager.instance.SetBallIndex (index);
		}
	}
}
