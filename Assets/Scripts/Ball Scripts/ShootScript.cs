﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq; // даст нам возможность конвертировать объекты

public class ShootScript : MonoBehaviour {


	public float power = 2.0f;
	public float life = 1.0f;
	public float dead_sense = 25f;

	public int dots = 30;

	private Vector2 startPosition;// проверка на движение мышки или тачскрин
	private bool shoot = false, aiming = false, hit_ground = false;
	private GameObject Dots;// родительская папка точек
	private List<GameObject> projectilesPath; // должно соединить все точки
	private Rigidbody2D myBody;
	private Collider2D myCollider;

	void Awake(){
		myBody = GetComponent<Rigidbody2D> ();
		myCollider = GetComponent<Collider2D> ();
	}
	// Use this for initialization
	void Start () {
		Dots = GameObject.Find ("dots");
		myBody.isKinematic = true;// при прицеливании на наш мяч не должна действовать физика
		myCollider.enabled=false;// и должен быть выключен коллайдер
		startPosition=transform.position;// когда коснемся мяча, увидем его положение и сохраним как начальное
		projectilesPath=Dots.transform.Cast<Transform> ().ToList ().ConvertAll (t=>t.gameObject);//помещаем всех детей 
		//объекта в список
		for (int i = 0; i < projectilesPath.Count; i++) {
			projectilesPath [i].GetComponent<Renderer> ().enabled = false;//прячем объект, до момента прицеливания. 
			//потом он снова появится

		}
	}
	
	// Update is called once per frame
	void Update () {
		Aim ();

		if (hit_ground) {
			life -= Time.deltaTime;
			Color c = GetComponent<Renderer> ().material.GetColor ("_Color");
			GetComponent<Renderer> ().material.SetColor ("_Color", new Color (c.r, c.g, c.b, life));
			if (life < 0) {

				if (GameManager.instance != null) {
					GameManager.instance.CreateBall ();
				}

				Destroy (gameObject);
			}
		}
	}
	void Aim(){//прицеливание
		if (shoot)
			return;

		if (Input.GetAxis ("Fire1") == 1) {//если зажата левая кнопка мыши, знаычит мы целимся
			if (!aiming) {
				aiming = true;
				startPosition = Input.mousePosition;
				CalculatePath ();
				ShowPath ();//эта функция для того, если мы зажали кнопку и целимся
			} else {
				CalculatePath ();// эта функция, если зажали кнопку и уже прицелились
			}
		} else if(aiming && !shoot){//проверка нажата ли кнопка
			if (inDeadZone (Input.mousePosition) || inReleaseZone (Input.mousePosition)) {
				//проверка если мы в мертвой зоне или за пределами броска, мы не можем бросить мяч
				aiming=false;
				HidePath ();
				return;//если мы в одной из этих зон, цикл прерывается и мы заново бросаем
			}
			myBody.isKinematic = false;
			myCollider.enabled = true;// хотим взаимодействовать с физикой
			shoot=true;
			aiming = false;
			myBody.AddForce (GetForce (Input.mousePosition));
			HidePath ();
			GameManager.instance.DecrementBalls ();
		}
	}
	Vector2 GetForce(Vector3 mouse){//работа мыши(на зажатие проверка)
		return (new Vector2(startPosition.x, startPosition.y) - new Vector2(mouse.x, mouse.y)) * power;
	}

	bool inDeadZone(Vector2 mouse){
		if (Mathf.Abs (startPosition.x - mouse.x) <= dead_sense && Mathf.Abs (startPosition.y - mouse.y) <= dead_sense) {
			return true;
		} else {
			return false;
		}
	}

	bool inReleaseZone(Vector2 mouse){
		if (mouse.x <= 70) {
			return true;
		}else{
			return false;
		}
	}

	void CalculatePath(){
		Vector2 vel = GetForce (Input.mousePosition) * Time.fixedDeltaTime / myBody.mass;// нам надо велосити,
		//чтобы показать, как сильно брошен мяч и его положение
		for (int i = 0; i < projectilesPath.Count; i++) {
			projectilesPath [i].GetComponent<Renderer> ().enabled = true;
			float t = i / 30f;//просчет времени
			Vector3 point = PathPoint (transform.position, vel, t);
			point.z = 1.0f;
			projectilesPath [i].transform.position = point;//формула подсчета направления
		}
	}

	Vector2 PathPoint(Vector2 StartP, Vector2 StartVel, float t){//использование начального положения, велосити и времени
		return StartP + StartVel * t + 0.5f * Physics2D.gravity * t * t;
	
	}

	void HidePath(){
		for (int i = 0; i < projectilesPath.Count; i++) {
			projectilesPath [i].GetComponent<Renderer> ().enabled = false;
		}
	}

	void ShowPath(){
		for (int i = 0; i < projectilesPath.Count; i++) {
			projectilesPath [i].GetComponent<Renderer> ().enabled = true;//таким образом мы показываем путь направления мяча
	}
		}
	void OnCollisionEnter2D(Collision2D target){
		if (target.gameObject.tag =="Ground") {
			hit_ground = true;
		}
	}
}//ShootScript
