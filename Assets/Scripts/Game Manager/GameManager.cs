﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	private int balls = 10;

	private int index = 0;
	private AudioSource audio;
	private float volume = 1.0f;

	[SerializeField]
	private AudioClip ram_hit1, ram_hit2, bounce1, bounce2, net_sound;


	private BallCreator ballCreator;//для связи между скриптами


	void Awake(){
		MakeSingleton ();
		audio = GetComponent<AudioSource> ();
		ballCreator = GetComponent<BallCreator> ();
	}

	void MakeSingleton(){
		if (instance != null) {
			Destroy (gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad (gameObject);
		}//это даст нам перенос объекта из одной сцены в другую
	}

	void OnLevelWasLoaded(){
		if (Application.loadedLevelName == "Gameplay") {
			CreateBall ();
		}
		GameObject.Find ("Ball Text").GetComponent<Text> ().text = "Balls " + balls;
	}

	public void IncrementBalls(int increment){
		balls += increment;
		if (balls > 10)
			balls = 10;
		GameObject.Find ("Ball Text").GetComponent<Text> ().text = "Balls " + balls;
	}
	public void DecrementBalls(){
		balls--;
		GameObject.Find ("Ball Text").GetComponent<Text> ().text = "Balls " + balls;
		if(balls <=0){
			DestroyObject (gameObject);
		}

	}
	public void CreateBall(){
		ballCreator.CreateBall (index);

	}
	public void SetBallIndex(int index){
		this.index = index;
	}

	public void PlaySound(int id){

		switch (id) {
		case 1:
			audio.PlayOneShot (net_sound, volume);
			break;
		case 2:
			if (Random.Range (0, 2) > 1) {
				audio.PlayOneShot (ram_hit1, volume);
			} else {
				audio.PlayOneShot (ram_hit2, volume);
			}
			break;
		case 3:
			if (Random.Range (0, 2) > 1) {
				audio.PlayOneShot (bounce1, volume);
			} else {
				audio.PlayOneShot (bounce2, volume);
			}
			break;
		case 4:
			if (Random.Range (0, 2) > 1) {
				audio.PlayOneShot (bounce1, volume / 2);
			} else {
				audio.PlayOneShot (bounce2, volume / 2);
			}
			break;
		case 5:
			if (Random.Range (0, 2) > 1) {
				audio.PlayOneShot (ram_hit1, volume / 2);
			} else {
				audio.PlayOneShot (ram_hit2, volume / 2);
			}
			break;
		}
	}
}//GameManager















































